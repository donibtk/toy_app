# Q&A

## 2.2.1
Q: (For readers who know CSS) Create a new user, then use your browser’s HTML inspector to determine the CSS id for the text “User was successfully created.” What happens when you refresh your browser?

A: id=notice; o elemento "p" com id=notice continua no documento, mas sem conteúdo.

Q: What happens if you try to create a user with a name but no email address?

A: O usuário é cadastrado sem email.

Q: What happens if you try create a user with an invalid email address, like “@example.com”?

A: O usuário é cadastrado com e-mail invalido.

Q: Destroy each of the users created in the previous exercises. Does Rails display a message by default when a user is destroyed?

A: Um alert pergunta se eu tenho certeza. Posso cancelar. Uma mensagem confirma que o usuário foi excluído. A mensagem é exibida no elemento "p#notice".

## 2.2.2
Q: By referring to Figure 2.11, write out the analogous steps for visiting the URL /users/1/edit.

A: Browser (/users/1/edit)-> Rails route -> users_controller.rb -> user.rb -> Database -> user.rb -> users_controller.rb -> edit.html.erb -> users_controller.rb -> browser.

Q: Find the line in the scaffolding code that retrieves the user from the database in the previous exercise.

A: @user = User.find(params[:id])

Q: What is the name of the view file for the user edit page?

A: edit.html.erb

## 2.3.1
Q: (For readers who know CSS) Create a new micropost, then use your browser’s HTML inspector to determine the CSS id for the text “Micropost was successfully created.” What happens when you refresh your browser?

A: id=notice; o elemento "p" com id=notice continua no documento, mas sem conteúdo.

## 2.3.2
Q: Try to create a micropost with the same long content used in a previous exercise (Section 2.3.1.1). How has the behavior changed?

A: O micropost não é criado e uma mensagem de erro é exibída.

Q: (For readers who know CSS) Use your browser’s HTML inspector to determine the CSS id of the error message produced by the previous exercise.

A: id=error_explanation (div)

## 2.3.4
Q: By examining the contents of the Application controller file, find the line that causes ApplicationController to inherit from ActionController::Base.

A: class ApplicationController < ActionController::Base

Q: Is there an analogous file containing a line where ApplicationRecord inherits from ActiveRecord::Base? Hint: It would probably be a file called something like application_record.rb in the app/models directory.

A: Sim: class ApplicationRecord < ActiveRecord::Base em application_record.rb.
